require([
    "esri/Map",
    "esri/views/SceneView",
    "esri/layers/GraphicsLayer",
    "esri/widgets/Sketch/SketchViewModel",
    "esri/symbols/WebStyleSymbol"
], function (
    Map,
    SceneView,
    GraphicsLayer,
    SketchViewModel,
    WebStyleSymbol
) {
    // the layer where the graphics are sketched
    const gLayer = new GraphicsLayer();

    const map = new Map({
        basemap: "satellite",
        layers: [gLayer],
        ground: "world-elevation"
    });

    const view = new SceneView({
        container: "viewDiv",
        map: map,
        camera: {
            //position: [9.76504392, 36.43538764, 2073.31548],
            position: {
                latitude: 30.15380554383376,
                longitude: 30.746404357390876,
                z: 548525.8886502236 // altitude in meters
            },
            heading: 359.9631470659164,
            tilt: 0.46042895205153084
        }
    });

    console.log(view);

    const blue = [82, 82, 122, 0.9];
    const white = [255, 255, 255, 0.8];

    // polygon symbol used for sketching the extruded building footprints
    const extrudedPolygon = {
        type: "polygon-3d",
        symbolLayers: [{
            type: "extrude",
            size: 10, // extrude by 10 meters
            material: {
                color: white
            },
            edges: {
                type: "solid",
                size: "3px",
                color: blue
            }
        }]
    };

    // polyline symbol used for sketching routes
    const route = {
        type: "line-3d",
        symbolLayers: [{
                type: "line",
                size: "3px",
                material: {
                    color: blue
                }
            },
            {
                type: "line",
                size: "10px",
                material: {
                    color: white
                }
            }
        ]
    };

    // point symbol used for sketching points of interest
    const point = {
        type: "point-3d",
        symbolLayers: [{
            type: "icon",
            size: "20px",
            resource: {
                primitive: "kite"
            },
            outline: {
                color: blue,
                size: "3px"
            },
            material: {
                color: white
            }
        }]
    };

    // define the SketchViewModel and pass in the symbols for each geometry type
    const sketchVM = new SketchViewModel({
        layer: gLayer,
        view: view,
        pointSymbol: point,
        polygonSymbol: extrudedPolygon,
        polylineSymbol: route
    });

    // after drawing the geometry, enter the update mode to update the geometry
    // and the deactivate the buttons
    sketchVM.on("create", function (event) {
        if (event.state === "complete") {
            sketchVM.update(event.graphic);
            deactivateButtons();
        }
    });

    const drawButtons = Array.prototype.slice.call(
        document.getElementsByClassName("esri-button")
    );

    // set event listeners to activate sketching graphics
    drawButtons.forEach(function (btn) {
        btn.addEventListener("click", function (event) {
            deactivateButtons();
            event.target.classList.add("esri-button--secondary");
            // to activate sketching the create method is called passing in the geometry type
            // from the data-type attribute of the html element
            sketchVM.create(event.target.getAttribute("data-type"));
        });
    });

    function deactivateButtons() {
        drawButtons.forEach(function (element) {
            element.classList.remove("esri-button--secondary");
        });
    }

    view.ui.add("sketchPanel", "top-right");
});